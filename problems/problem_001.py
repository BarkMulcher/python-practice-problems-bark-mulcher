# Complete the minimum_value function so that returns the
# minimum of two values.
#
# If the values are the same, return either.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

# define function
def minimum_value(value1, value2):
    # compare two values
    # if statement comparing two arguments
    if value1 < value2:
        # returns value1 if it is minimum
        return value1
    else:
        # returns value2 if it is minimum
        return value2

print(minimum_value(65,56))
