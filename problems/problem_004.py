# Complete the max_of_three function so that returns the
# maximum of three values.
#
# If two values are the same maximum value, return either of
# them.
# If the all of the values are the same, return any of them
#
# Use the >= operator for greater than or equal to

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def max_of_three(value1, value2, value3):
    # compare 3 values
    # if statement comparing v1/v2/v3
    # if v1 > v2 > v3,
    if value1 >= value2 and value1 >= value3:
        # then v1 > v3
        return value1
    # else if v2 <= v1/v3
    elif value2 >= value1 and value2 >= value3:
        return value2
    else:
        return value3
    # # if statement comparing v2/v3
    # if value2 >= value3:
    #     # nested if statement to see if they are equal
    #     if value2 == value3:
    #         # return
    #         return value3
    #     # return
    #     else:
    #         return value2
    # # if v1 > v2 and v2 > v3,
    # # then v1 > v3
    # if value1 > value2 and value2 > value3:
    #     return value1

print(max_of_three(111, 28, 35))

# if 1 > 2 > 3, then 1 > 3
#