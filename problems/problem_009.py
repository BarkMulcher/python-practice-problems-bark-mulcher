# Complete the is_palindrome function to check if the value in
# the word parameter is the same backward and forward.
#
# For example, the word "racecar" is a palindrome because, if
# you write it backwards, it's the same word.

# It uses the built-in function reversed and the join method
# for string objects.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def is_palindrome(word):
    # create container for reversed
    # use .reverse() function
    rev_word = reversed(word)
    # use .join() method
    joined_rev_word = ''.join(rev_word)
    print(joined_rev_word)
    # compare
    if joined_rev_word == word:
        print('It is a palindrome')
    else:
        print('It is not a palindrome')

is_palindrome("race car")
