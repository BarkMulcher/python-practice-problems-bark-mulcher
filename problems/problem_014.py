# Complete the can_make_pasta function to
# * Return true if the ingredients list contains
#   "flour", "eggs", and "oil"
# * Otherwise, return false
#
# The ingredients list will always contain three items.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

# define ingredients list
# iterate over ingredients list
# check if needed ingredients are in list
# conditionals for returning T/F
    # if oil, flour, AND eggs, True
    # else, false

ingredients = ['flour', 'eggs', 'oil']

def can_make_pasta(ingredients):
    for items in ingredients:
        if 'oil' in ingredients:
            oil_in_list = True
        else:
            oil_in_list = False
        if 'flour' in ingredients:
            flour_in_list = True
        else:
            flour_in_list = False
        if 'eggs' in ingredients:
            eggs_in_list = True
        else:
            eggs_in_list = False
    if oil_in_list == True and flour_in_list == True and eggs_in_list == True:
        print(True)
    else:
        print(False)

can_make_pasta(ingredients)