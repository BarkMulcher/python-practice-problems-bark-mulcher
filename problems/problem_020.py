# Complete the has_quorum function to test to see if the number
# of the people in the attendees list is greater than or equal
# to 50% of the number of people in the members list.

# create variable that expresses what half of the member list would be
# create conditional that compares attendee list to the halved member list variable
# print T/F based on those conditions
# call function

def has_quorum(attendees_list, members_list):
    half_member_list = members_list / 2

    if attendees_list >= half_member_list:
        print(True)
    else:
        print(False)

has_quorum(9, 20)