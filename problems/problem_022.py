# Complete the gear_for_day function which returns a list of
# gear needed for the day given certain criteria.
#   * If the day is not sunny and it is a workday, the list
#     needs to contain "umbrella"
#   * If it is *ONLY* (my addition) a workday, the list needs to contain "laptop"
#   * If it is not a workday, the list needs to contain *ONLY* (my addition)
#     "surfboard"

# create gear list
# add gear depending on if a workday
# use .append()

def gear_for_day(is_workday, is_sunny):
    gear_lst = []
    if is_workday == True and is_sunny == False:
        gear_lst.extend(['laptop', 'umbrella'])
        print(gear_lst)
    if is_workday == True and is_sunny == True:
        gear_lst.append('laptop')
        print(gear_lst)
    if is_workday == False:
        gear_lst.append('surfboard')
        print(gear_lst)

gear_for_day(False, False)
