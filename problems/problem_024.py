# Complete the calculate_average function which accepts
# a list of numerical values and returns the average of
# the numbers.
#
# If the list of values is empty, the function should
# return None
#
# Pseudocode is available for you

# create empty list named 'values'
# create a container variable to contain a sum
# have a sum function
# assign the sum to the container variable
# use len() to find the number of values in 'values'
# create variable for average represented by:
# divide sum by len()
# print average
def calculate_average(values):
    total = 0

    if len(values) == 0:
        print(None)
    else:
        for nums in values:
            total += nums

        print(total)

        average = total / len(values)
        print(average)



values = []

calculate_average(values)

# OR

def calc_avg_w_sum(valuez):
    total = sum(valuez)
    print(total)
    if len(valuez) == 0:
        print(None)
    else:
        avg = total / len(valuez)
        print(avg)

valuez = []

calc_avg_w_sum(valuez)