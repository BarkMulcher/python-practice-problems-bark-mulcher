# Complete the calculate_sum function which accepts
# a list of numerical values and returns the sum of
# the numbers.
#
# If the list of values is empty, the function should
# return None
#
# simple version: use sum() function
# long way: sum with container:
# create container variable "total" initialized to zero
# create for loop to iterate over values list
# iterate over & add values w/ +=
# print new total

def calculate_sum(values):
    total = 0
    for nums in values:
        total += nums
    print(total)

values = [1,2,3]

calculate_sum(values)

# OR

def calc_sum(valuez):
    total = sum(valuez)
    print(total)

valuez = [2,3]

calc_sum(valuez)
