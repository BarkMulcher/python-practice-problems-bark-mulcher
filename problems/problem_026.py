# Complete the calculate_grade function which accepts
# a list of numerical scores each between 0 and 100.
#
# Based on the average of the scores, the function
# returns
#   * An "A" for an average greater than or equal to 90
#   * A "B" for an average greater than or equal to 80
#     and less than 90
#   * A "C" for an average greater than or equal to 70
#     and less than 80
#   * A "D" for an average greater than or equal to 60
#     and less than 70
#   * An "F" for any other average
#
# create empty list named "values"
# create container variable "total" for the sum
# iterate over values and perform sum
# use len() to find number of values in "values"
# create variable for average represented by
# total divided by len(values)
# assign letter grade to numbers
# do so with conditionals

def calculate_grade(values):
    total = 0

    for nums in values:
        total += nums
        print(total)
    avg = total / len(values)
    print(avg)

    grade = []
    if avg < 60:
            grade = "F"
    if avg >= 60 and avg < 70:
            grade = "D"
    if avg >= 70 and avg < 80:
            grade = "C"
    if avg >= 80 and avg < 90:
            grade = "B"
    if avg >= 90:
            grade = "A"
    print(grade)

values = [90, 65, 90]

calculate_grade(values)