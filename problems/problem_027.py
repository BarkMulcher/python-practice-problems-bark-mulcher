# Complete the max_in_list function to find the
# maximum value in a list
#
# If the list is empty, then return None.
#
# index values
# by iterating over and using index?
# compare values
# print max value by index
# or sort ascending and print last index value - did this one

def max_in_list(values):

    if len(values) == 0:
        print(None)

    else:
        # sort ascending
        values.sort()
    # print last value in list (represented by index - 1 because they start at zero(?))
    print(values[-1])


values = [12988, 1, 7, 12, 688888]

max_in_list(values)


# OR

def macks_in_list(values):
    if len(values) == 0:
        print(None)
    else:
        for i in values:
            max_num = values[i]
