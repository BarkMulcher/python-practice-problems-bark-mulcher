# Complete the remove_duplicate_letters that takes a string
# parameter "s" and returns a string with all of the
# duplicates removed.
# Examples:
#   * For "abc", the result is "abc"
#   * For "abcabc", the result is "abc"
#   * For "abccba", the result is "abc"
#   * For "abccbad", the result is "abcd"#
# If the list is empty, then return the empty string.

# a few ways to do this:
# separate/transform string "s" into a list with split()
# iterate over new list with containing checker "in"

# create frequency container variable?
# use function count() to count letters in string
# count number of each letter
# conditional for if frequency > 1
# if greater than 1, use delete function?
# print modified string

# create new string variable
# iterate over new string
# if new string doesn't contain a letter
# from original string 's',
# then add it with +=
# print new string

# def remove_duplicate_letters(s):
#     # if list is empty:
#     if len(s) == 0:
#         # print empty string
#         print("")
#     else:
#         for i, letter in enumerate(s):
#             print(i, letter)
#             str_count = s.count(letter)
#             if str_count > 1:
#                 print(True)
#         print(str_count)
# def remove_duplicate_letters(s):
#     nu_string = ''
#     for chars in s:
#         if chars.count(nu_string) == 0:
#             nu_string += chars
#     print(nu_string)
def remove_duplicate_letters(s):
    nu_string = ''
    for chars in s:
        if chars not in nu_string:
            nu_string += chars
    print(nu_string)


s = "aaaabcdefgggghhhh"

remove_duplicate_letters(s)



