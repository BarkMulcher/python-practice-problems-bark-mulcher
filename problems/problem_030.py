# Complete the find_second_largest function which accepts
# a list of numerical values and returns the second largest
# in the list
#
# If the list of values is empty, the function should
# return None
#
# If the list of values has only one value, the function
# should return None
#
# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

# rule out any lists that have one or none balues
# sort the list
# print the balue with index -2
# there is a more professional way I'm sure

def find_second_largest(values):
    if len(values) <= 1:
        print(None)
    else:
        values.sort()
        print(values)
        print(values[-2])

values = [0, 1, 2, 3, 99, 66, 88, 4, -2, 77, 100, 2212, 44, 102]
find_second_largest(values)
