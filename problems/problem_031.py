# Complete the sum_of_squares function which accepts
# a list of numerical values and returns the sum of
# each item squared
#
# If the list of values is empty, the function should
# return None
#
# Examples:
#   * [] returns None
#   * [1, 2, 3] returns 1*1+2*2+3*3=14
#   * [-1, 0, 1] returns (-1)*(-1)+0*0+1*1=2

# create new empty list variable for squared values
# exclude empty list
# convert
# iterate over values
# perform squaring operation ** ...?

# use += somewhere
# append squared values to new list?
# or replace?
# print new list

def sum_of_squares(values):
    vals_sqd = []
    if len(values) == 0:
        print(None)
    else:
        for nums in values:
            vals_sqd.append(nums ** 2)
    print(vals_sqd)

values = [2, 3, 4]
sum_of_squares(values)