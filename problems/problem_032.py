# Complete the sum_of_first_n_numbers function which accepts
# a numerical limit and returns the sum of the numbers from
# 0 up to and including limit.
#
# If the value of the limit is less than 0, then it should
# return None
#
# Examples:
#   * -1 returns None
#   * 0 returns 0
#   * 1 returns 0+1=1
#   * 2 returns 0+1+2=3
#   * 5 returns 0+1+2+3+4+5=15
# lol attempt
# exclude negative index value
# define new empty list for sum
# iterate over list items up to the limit
# append numbers between 0 - limit
# using:
# range(len(limit)) as maximum
#
# print sum

def sum_of_first_n_numbers(limit):
    limit_sum = 0
    if limit < 0:
        print(None)
    else:
        # 0, lim not necessary. can be only "limit + 1"
        #  the + 1 part is referring to adding 1, not the index
        # don't understand that part
        for nums in range(limit + 1):
            limit_sum += nums

        print(limit_sum)

limit = 5
sum_of_first_n_numbers(limit)