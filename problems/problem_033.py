# Complete the sum_of_first_n_even_numbers function which
# accepts a numerical count n and returns the sum of the
# first n even numbers
#
# If the value of the limit is less than 0, then it should
# return None
# Examples:
#   * -1 returns None
#   * 0 returns 0
#   * 1 returns 0+2=2
#   * 2 returns 0+2+4=6
#   * 5 returns 0+2+4+6+8+10=30

# again define empty container list
# exclude < 0 value in "n"
# if limit is even,
# summate to index of limit? how?
# if limit is odd,
# summate to index of limit -1?
# print new container

def sum_of_first_n_even_numbers(n):
    even_sum = 0
    if n < 0:
        print(None)
    else:
        for i in range(n + 1):
            even_sum += i * 2
        print(even_sum)

n = 1
sum_of_first_n_even_numbers(n)