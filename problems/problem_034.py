# Complete the count_letters_and_digits function which
# accepts a parameter s that contains a string and returns
# two values, the number of letters in the string and the
# number of digits in the string
#
# Examples:
#   * "" returns 0, 0
#   * "a" returns 1, 0
#   * "1" returns 0, 1
#   * "1a" returns 1, 1
#
# To test if a character c is a digit, you can use the
# c.isdigit() method to return True of False
#
# To test if a character c is a letter, you can use the
# c.isalpha() method to return True of False
#
# Remember that functions can return more than one value
# in Python. You just use a comma with the return, like
# this:
#      return value1, value2
# s is a string
# iterate over s
# use len(s) to return number of letters
# use len(s) again to return number of digits

def count_letters_and_digits(s):
    charcount_alpha = 0
    charcount_digit = 0
    if len(s) == 0:
        return 0, 0
    else:
        for chars in s:
            if chars.isalpha():
                charcount_alpha += 1
            if chars.isdigit():
                charcount_digit += 1
        print(charcount_alpha, charcount_digit)


s = "aa22222"
count_letters_and_digits(s)