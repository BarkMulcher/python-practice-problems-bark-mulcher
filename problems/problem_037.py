# Complete the pad_left function which takes three parameters
#   * a number
#   * the number of characters in the result
#   * a padding character
# and turns the number into a string of the desired length
# by adding the padding character to the left of it
#
# Examples:
#   * number: 10
#     length: 4
#     pad:    "*"
#     result: "**10"
#   * number: 10
#     length: 5
#     pad:    "0"
#     result: "00010"
#   * number: 1000
#     length: 3
#     pad:    "0"
#     result: "1000"
#   * number: 19
#     length: 5
#     pad:    " "
#     result: "   19"
# make container variable
# it will hold padding & numbers
# add number to container
# reference length to determine the number of *
# number of *s + len(number) = length
# add asterisks before

def pad_left(number, length, pad):
    # container = ''
    # container += str(number)
    number_of_pads = length - len(str(number))
    num_str = number_of_pads*pad
    result = num_str + str(number)
    print(result)



pad_left(100, 15, '0')
