# Complete the reverse_dictionary function which has a
# single parameter that is a dictionary. Return a new
# dictionary that has the original dictionary's values
# for its keys, and the original dictionary's keys for
# its values.
#
# Examples:
#   * input:  {}
#     output: {}
#   * input:  {"key": "value"}
#     output: {"value", "key"}
#   * input:  {"one": 1, "two": 2, "three": 3}
#     output: {1: "one", 2: "two", 3: "three"}

def reverse_dictionary(dictionary):
    # make empty dictionary
    reverse_dict = dict()
    # loop over original dictionary
    for key in dictionary:
        # set key:value pairs
        val = dictionary[key]
        # insert reversed value:key pairs into empty dict
        reverse_dict[val] = key
    print(reverse_dict)

reverse_dictionary({'fart': 'poop'})
