# Write a class that meets these requirements.
#
# Name:       Book
#
# Required state:
#    * author name, a string
#    * title, a string
#
class Book:
    def __init__(self, author, title):
        self.author = author
        self.title = title

    def get_author(self):
        return 'Author: ' + self.author

    def get_title(self):
        return 'Title: ' + self.title

# example:

book = Book('Delicious Tacos', 'The Pussy')

print(book.get_author())
print(book.get_title())