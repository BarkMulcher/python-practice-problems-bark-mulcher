# Write a class that meets these requirements.
#
# Name:       Person
#
# Required state:
#    * name, a string
#    * hated foods list, a list of names of food they don't like
#    * loved foods list, a list of names of food they really do like
#
# Behavior:
#    * taste(food name)  * returns None if the food name is not in their
#                                  hated or loved food lists
#                        * returns True if the food name is in their
#                                  loved food list
#                        * returns False if the food name is in their
#                                  hated food list

class Person:
    def __init__(self, name, hated_foods, loved_foods):
        self.name = name
        self.hated_foods = hated_foods
        self.loved_foods = loved_foods

    def taste(self, food):
        if food in self.hated_foods:
            return 'Luke hates that'
        elif food in self.loved_foods:
            return 'Luke loves that'
        else:
            return 'what'


# example:

person = Person('Luke',
                ['okra', 'poop', 'bleu cheese'],
                ['pizza', 'carrots'])
print(person.taste('poop'))