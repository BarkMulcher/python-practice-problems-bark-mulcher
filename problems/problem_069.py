# Write a class that meets these requirements.
#
# Name:       Student
#
# Required state:
#    * name, a string
#
# Behavior:
#    * add_score(score)   # Adds a score to their list of scores
#    * get_average()      # Gets the average of the student's scores
#
# Example:
#    student = Student("Malik")
#
#    print(student.get_average())    # Prints None
#    student.add_score(80)
#    print(student.get_average())    # Prints 80
#    student.add_score(90)
#    student.add_score(82)
#    print(student.get_average())    # Prints 84
#
# There is pseudocode for you to guide you.


class Student:
    def __init__(self, name):
        self.name = name
        self.scores = []

    def add_score(self, score):
        return self.scores.append(score)

    def get_avg(self):
        total = sum(self.scores)
        score_total = len(self.scores)

        if len(self.scores) == 0:
            return None
        else:
            for scores in self.scores:
                return total / score_total

student = Student('Luke')

print(student.get_avg())
student.add_score(80)
print(student.get_avg())
student.add_score(95)
student.add_score(65)
print(student.get_avg())
student.add_score(99)
print(student.get_avg())