# Write a class that meets these requirements.
#
# Name:       Student
#
# Required state:
#    * name, a string
#
# Behavior:
#    * add_score(score)   # Adds a score to their list of scores
#    * get_average()      # Gets the average of the student's scores
#
# Example:
#    student = Student("Malik")
#
#    print(student.get_average())    # Prints None
#    student.add_score(80)
#    print(student.get_average())    # Prints 80
#    student.add_score(90)
#    student.add_score(82)
#    print(student.get_average())    # Prints 84
#
# define init with self, name parameters
# within that, define name as name and score as empty list
# define add_score with self & score parameter
# use append() & return
# define get_avg with self parameter
# use conditional to return None if no scores
# if scores exist, use sum and divide by len(scores)

class Student:
    def __init__(self, name):
        self.name = name
        self.scores = []

    def add_score(self, score):
        self.scores.append(score)
        return self.scores

    def get_avg(self):
        if len(self.scores) == 0:
            return None
        else:
            for scores in self.scores:
                avg = sum(self.scores) / len(self.scores)
                return avg

student = Student('Luke')

print(student.get_avg())
student.add_score(95)
student.add_score(85)
student.add_score(60)
print(student.get_avg())


