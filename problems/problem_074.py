# Write a class that meets these requirements.
#
# Name:       BankAccount
#
# Required state:
#    * opening balance, the amount of money in the bank account
#
# Behavior:
#    * get_balance()      # Returns how much is in the bank account
#    * deposit(amount)    # Adds money to the current balance
#    * withdraw(amount)   # Reduces the current balance by amount
# Example:
#    account = BankAccount(100)
#
#    print(account.get_balance())  # prints 100
#    account.withdraw(50)
#    print(account.get_balance())  # prints 50
#    account.deposit(120)
#    print(account.get_balance())  # prints 170
#
# define BankAccount class
# initialize with self, opening balance
# define get balance function with self, balance parameters
# return balance
# define deposit fn with self, dep_amt parameters
# increment +=
# return addition to balance
# define withdraw fn with self withdrawn_amt parameters
# decrement -=
# return subtraction from balance

class BankAccount:
    def __init__(self, opening_balance):
        self.balance = opening_balance

    def get_balance(self):
        return self.balance

    def deposit(self, dep_amt):
        self.balance += dep_amt
        return self.balance

    def withdraw(self, withdrawn_amt):
        self.balance -= withdrawn_amt
        return self.balance

bankaccount = BankAccount(100)
print(bankaccount.get_balance())
bankaccount.deposit(100)
print(bankaccount.get_balance())
bankaccount.withdraw(50)
print(bankaccount.get_balance())