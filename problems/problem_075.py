# Write a class that meets these requirements.
#
# Name:       BankAccount
#
# Required state:
#    * opening balance, the amount of money in the bank account
#
# Behavior:
#    * get_balance()      # Returns how much is in the bank account
#    * deposit(amount)    # Adds money to the current balance
#    * withdraw(amount)   # Reduces the current balance by amount
#
# Example:
#    account = BankAccount(100)
#
#    print(account.get_balance())  # prints 100
#    account.withdraw(50)
#    print(account.get_balance())  # prints 50
#    account.deposit(120)
#    print(account.get_balance())  # prints 170
#
# define class BankAccount with parameters self, opening_balance
# define check bal fn with params self
# def deposit fn with params self, dep amt
# increment += balance
# def withdrawal fn w/ params self, wdrw amt
# decrement -= bal

class BankAccount:
    def __init__(self, opening_bal):
        self.balance = opening_bal

    def check_bal(self):
        return self.balance

    def deposit(self, dep_amt):
        self.balance += dep_amt
        return self.balance

    def withdraw(self, wdrw_amt):
        self.balance -= wdrw_amt
        return self.balance

account = BankAccount(100)
print(account.check_bal())
account.withdraw(200)
print(account.check_bal())



