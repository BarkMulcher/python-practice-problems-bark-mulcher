# Write a class that meets these requirements.
#
# Name:       Circle
#
# Required state:
#    * radius, a non-negative value
#
# Behavior:
#    * calculate_perimeter()  # Returns the length of the perimeter of the circle
#    * calculate_area()       # Returns the area of the circle
#
# Example:
#    circle = Circle(10)
#
#    print(circle.calculate_perimeter())  # Prints 62.83185307179586
#    print(circle.calculate_area())       # Prints 314.1592653589793
#

import math

class Circle:
    def __init__(self, radius):
        if radius < 0:
            return ValueError
        self.radius = radius

    def calc_perim(self):
        perimeter = math.pi * 2 * self.radius
        return perimeter

    def calc_area(self):
        area = math.pi * (self.radius ** 2)
        return area

circle = Circle(1)

print(circle.calc_area())
